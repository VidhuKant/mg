/* mg - MyAnimeList to Go API wrapper
 * Copyright (C) 2023  Vidhu Kant Sharma <vidhukant@vidhukant.com>

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package mg

import (
	"strings"
	"strconv"
	"slices"
)

func validateAnimeFields(fields *[]string) error {
	// if no fields given, set all
	if cap(*fields) == 0 {
		*fields = DefaultAnimeFields
		return nil
	}

	// check if given fields exist in DefaultAnimeFields
	for _, f := range *fields {
		if !slices.Contains(DefaultAnimeFields, f) {
			return ErrInvalidField
		}
	}

	return nil
}

func validateAnimeRankingType(t string) error {
	if !slices.Contains([]string{
		RankingTypeAll,
		RankingTypeByPopularity,
		RankingTypeFavorite,
		RankingTypeAiring,
		RankingTypeUpcoming,
		RankingTypeTV,
		RankingTypeOVA,
		RankingTypeMovie,
		RankingTypeSpecial,
	}, t) {
		return ErrInvalidRankingType
	}

	return nil
}

func validateAnimeListSort(s string) error {
	if !slices.Contains([]string{
		SortByListScore,
		SortByListUpdatedAt,
		SortByAnimeTitle,
		SortByAnimeStartDate,
		SortByAnimeId,
	}, s) {
		return ErrInvalidSort
	}

	return nil
}

func validateAnimeListStatus(s string) error {
	if !slices.Contains([]string{
		ListStatusCompleted,
		ListStatusOnHold,
		ListStatusDropped,
		ListStatusWatching,
		ListStatusPTW,
	}, s) {
		return ErrInvalidStatus
	}

	return nil
}

func validateAnimeSeason(s string) error {
	if !slices.Contains([]string{
		SeasonWinter,
		SeasonSpring,
		SeasonSummer,
		SeasonFall,
	}, s) {
		return ErrInvalidSeason
	}

	return nil
}

func validateAnimeSeasonSort(s string) error {
	if !slices.Contains([]string{
		SeasonSortByAnimeScore,
		SeasonSortByNumListUsers,
	}, s) {
		return ErrInvalidSort
	}

	return nil
}

func validateMangaFields(fields *[]string) error {
	// if no fields given, set all
	if cap(*fields) == 0 {
		*fields = DefaultMangaFields
		return nil
	}

	// check if given fields exist in DefaultAnimeFields
	for i, f := range *fields {
		if f == "authors" {
			f = "authors{first_name,last_name}"
			x := *fields
			x[i] = f
			*fields = x
		}

		if !slices.Contains(DefaultMangaFields, f) {
			return ErrInvalidField
		}
	}

	return nil
}

func validateMangaRankingType(t string) error {
	if !slices.Contains([]string{
		RankingTypeAll,
		RankingTypeByPopularity,
		RankingTypeFavorite,
		RankingTypeManga,
		RankingTypeNovel,
		RankingTypeOneShot,
		RankingTypeDoujin,
		RankingTypeManhwa,
		RankingTypeManhua,
	}, t) {
		return ErrInvalidRankingType
	}

	return nil
}

func validateMangaListSort(s string) error {
	if !slices.Contains([]string{
		SortByListScore,
		SortByListUpdatedAt,
		SortByMangaTitle,
		SortByMangaStartDate,
		SortByMangaId,
	}, s) {
		return ErrInvalidSort
	}

	return nil
}

func validateMangaListStatus(s string) error {
	if !slices.Contains([]string{
		ListStatusCompleted,
		ListStatusOnHold,
		ListStatusDropped,
		ListStatusReading,
		ListStatusPTR,
	}, s) {
		return ErrInvalidStatus
	}

	return nil
}

func validateScore(score int) error {
	if score >= 0 && score <= 10 {
		return nil
	}

	return ErrInvalidScore
}

func validatePriority(priority int) error {
	if priority >= 0 && priority <= 2 {
		return nil
	}

	return ErrInvalidPriority
}

func validateRewatchValue(val int) error {
	if val >= 0 && val <= 5 {
		return nil
	}

	return ErrInvalidRewatchValue
}

func validateRereadValue(val int) error {
	if val >= 0 && val <= 5 {
		return nil
	}

	return ErrInvalidRereadValue
}

func validateAnimeSearchParams(params *SearchParams) error {
	if strings.TrimSpace(params.SearchString) == "" {
		return ErrEmptySearchString
	}

	if params.Limit > 100 || params.Limit < 1 {
		return ErrLimitOutOfRange
	}

	err := validateAnimeFields(&params.Fields)

	return err
}

func validateAnimeRankingParams(params *RankingParams) error {
	err := validateAnimeRankingType(params.RankingType)
	if err != nil {
		return err
	}

	if params.Limit > 500 || params.Limit < 1 {
		return ErrLimitOutOfRange
	}

	err = validateAnimeFields(&params.Fields)

	return err
}

func validateSeasonalParams(params *SeasonalParams) error {
	// check if year is int
	_, err := strconv.Atoi(params.Year)
	if err != nil {
		return ErrInvalidYear
	}

	err = validateAnimeSeason(params.Season)
	if err != nil {
		return err
	}

	err = validateAnimeSeasonSort(params.Sort)
	if err != nil {
		return err
	}

	if params.Limit > 500 || params.Limit < 1 {
		return ErrLimitOutOfRange
	}

	err = validateMangaFields(&params.Fields)

	return err
}

func validateSuggestedParams(params *SuggestedParams) error {
	if params.Limit > 100 || params.Limit < 1 {
		return ErrLimitOutOfRange
	}

	return validateAnimeFields(&params.Fields)
}

func validateAnimeListParams(params *ListParams) error {
	// if username is empty, set it to @me
	if strings.TrimSpace(params.Username) == "" {
		params.Username = "@me"
	}

	if params.Status != "" {
		err := validateAnimeListStatus(params.Status)
		if err != nil {
			return err
		}
	}

	err := validateAnimeListSort(params.Sort)
	if err != nil {
		return err
	}

	if params.Limit > 1000 || params.Limit < 1 {
		return ErrLimitOutOfRange
	}

	return validateAnimeFields(&params.Fields)
}

func validateMangaSearchParams(params *SearchParams) error {
	if strings.TrimSpace(params.SearchString) == "" {
		return ErrEmptySearchString
	}

	if params.Limit > 100 || params.Limit < 1 {
		return ErrLimitOutOfRange
	}

	err := validateMangaFields(&params.Fields)

	return err
}

func validateMangaRankingParams(params *RankingParams) error {
	err := validateMangaRankingType(params.RankingType)
	if err != nil {
		return err
	}

	if params.Limit > 500 || params.Limit < 1 {
		return ErrLimitOutOfRange
	}

	err = validateMangaFields(&params.Fields)

	return err
}

func validateMangaListParams(params *ListParams) error {
	// if username is empty, set it to @me
	if strings.TrimSpace(params.Username) == "" {
		params.Username = "@me"
	}

	if params.Status != "" {
		err := validateMangaListStatus(params.Status)
		if err != nil {
			return err
		}
	}

	err := validateMangaListSort(params.Sort)
	if err != nil {
		return err
	}

	if params.Limit > 1000 || params.Limit < 1 {
		return ErrLimitOutOfRange
	}

	return validateMangaFields(&params.Fields)
}
