/* mg - MyAnimeList to Go API wrapper
 * Copyright (C) 2023  Vidhu Kant Sharma <vidhukant@vidhukant.com>

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package mg

type MALError struct {
	Err string `json:"error"`
	Msg string `json:"message"`
}

type Picture struct {
	Medium string `json:"medium"`
	Large  string `json:"large"`
}

type StatusStatistics struct {
	Watching    int `json:"watching"`
	Completed   int `json:"completed"`
	OnHold      int `json:"on_hold"`
	Dropped     int `json:"dropped"`
	PlanToWatch int `json:"plan_to_watch"`
}

type Genre struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

type AltTitles struct {
	Synonyms      []string `json:"synonyms"`
	EnglishTitle  string   `json:"en"`
	JapaneseTitle string   `json:"ja"`
}

type listStatus struct {
	Status     string `json:"status"`
	Score      int    `json:"score"`
	StartDate  string `json:"start_date"`
	FinishDate string `json:"finish_date"`
	Priority   int    `json:"priority"`
	Tags       string `json:"tags"`
	Comments   string `json:"comments"`
	UpdatedAt  string `json:"updated_at"`
}
