/* mg - MyAnimeList to Go API wrapper
 * Copyright (C) 2023  Vidhu Kant Sharma <vidhukant@vidhukant.com>

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package mg

import (
	"encoding/json"
	"net/url"
	"fmt"
)

type AnimeUpdateResponse struct {
	Status         string `json:"status"`
	Score          int    `json:"score"`
	EpWatched      int    `json:"num_episodes_watched"`
	IsRewatching   bool   `json:"is_rewatching"`
	StartDate      string `json:"start_date"`
	FinishDate     string `json:"finish_date"`
	Priority       string `json:"priority"`
	TimesRewatched int    `json:"num_times_rewatched"`
	RewatchValue   int    `json:"rewatch_value"`
	Tags           string `json:"tags"`
	Comments       string `json:"comments"`
	UpdatedAt      string `json:"updated_at"`
}

func (c Client) DeleteAnime(id int) error {
	return c.delete(ANIME_BASE_URL + getDeleteQuery(id))
}

func (c Client) GetAnimeList(animes *[]Anime, params *ListParams) (bool, error) {
	err := validateAnimeListParams(params)
	if err != nil {
		return false, err
	}

	params.Fields = append(params.Fields, "list_status")

	var res struct {
		Data []struct {
			Anime      Anime           `json:"node"`
			ListStatus AnimeListStatus `json:"list_status"`
		} `json:"data"`
		Paging struct {
			NextPage string `json:"next"`
			// PrevPage string `json:"previous"`
		} `json:"paging"`
	}

	body, err := c.get(BASE_URL + getListQuery(params, "anime"))
	if err != nil {
		return false, err
	}
	json.Unmarshal(body, &res)

	for _, el := range res.Data {
		el.Anime.ListStatus = el.ListStatus
		*animes = append(*animes, el.Anime)
	}

	// returns true if there is a next page
	return res.Paging.NextPage != "", nil
}

func (c Client) UpdateAnime(res *AnimeUpdateResponse, id int, params map[string]interface{}) error {
	p := url.Values{}
	for k, v := range params {
		vString := fmt.Sprint(v)
		var err error

		// validate params
		switch(k) {
		case Status:
			err = validateAnimeListStatus(vString)
		case Score:
			err = validateScore(v.(int))
		case Priority:
			err = validatePriority(v.(int))
		case RewatchValue:
			err = validateRewatchValue(v.(int))
		case Tags, Comments, IsRewatching, EpisodesWatched, TimesRewatched:
			// these ones don't need to be validated
			err = nil
		default:
			err = ErrUnknownUpdateParam
		}

		if err != nil {
			return err
		}

		p.Set(k, vString)
	}

	body, err := c.put(getUpdateQuery(id, "anime"), p)
	if err != nil {
		return err
	}
	json.Unmarshal(body, &res)

	return nil
}
