/* mg - MyAnimeList to Go API wrapper
 * Copyright (C) 2023  Vidhu Kant Sharma <vidhukant@vidhukant.com>

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package mg

import "encoding/json"

const ANIME_BASE_URL string = BASE_URL + "/anime"

func (c Client) SearchAnime(animes *[]Anime, params *SearchParams) error {
	err := validateAnimeSearchParams(params)
	if err != nil {
		return err
	}

	var res struct {
	  Data []struct {
	  	Anime Anime `json:"node"`
	  } `json:"data"`
	}

	body, err := c.get(ANIME_BASE_URL + getSearchQuery(params))
	if err != nil {
		return err
	}
	json.Unmarshal(body, &res)

	for _, el := range res.Data {
		*animes = append(*animes, el.Anime)
	}

	return nil
}

func (c Client) GetAnimeById(anime *Anime, id int, fields []string) error {
	err := validateAnimeFields(&fields)
	if err != nil {
		return err
	}

	body, err := c.get(ANIME_BASE_URL + getIdQuery(id, fields))
	if err != nil {
		return err
	}
	json.Unmarshal(body, anime)

	return nil
}

func (c Client) GetAnimeRanking(animes *[]RankedAnime, params *RankingParams) error {
	err := validateAnimeRankingParams(params)
	if err != nil {
		return err
	}

	body, err := c.get(ANIME_BASE_URL + getRankingQuery(params))
	if err != nil {
		return err
	}

	var res struct {
	  Data []struct {
	  	Anime RankedAnime `json:"node"`
	  	Ranking struct {
	  		Rank int `json:"rank"`
	  	} `json:"ranking"`
	  } `json:"data"`
  }
	json.Unmarshal(body, &res)

	for _, el := range res.Data {
		el.Anime.RankNum = el.Ranking.Rank
		*animes = append(*animes, el.Anime)
	}

	return nil
}

func (c Client) GetSeasonalAnime(animes *[]Anime, params *SeasonalParams) error {
	err := validateSeasonalParams(params)
	if err != nil {
		return err
	}

	body, err := c.get(ANIME_BASE_URL + getSeasonalQuery(params))
	if err != nil {
		return err
	}

	var res struct {
		Data []struct {
			Anime Anime `json:"node"`
		} `json:"data"`
		// Season Season `json:"season"`
	}
	json.Unmarshal(body, &res)

	for _, el := range res.Data {
		*animes = append(*animes, el.Anime)
	}

	return nil
}

func (c Client) GetSuggestedAnime(animes *[]Anime, params *SuggestedParams) error {
	if c.ClientAuthOnly {
		// this endpoint requires main auth
		return ErrClientAuthNotSupported
	}

	err := validateSuggestedParams(params)
	if err != nil {
		return err
	}

	body, err := c.get(ANIME_BASE_URL + getSuggestedQuery(params))
	if err != nil {
		return err
	}

	var res struct {
		Data []struct {
			Anime Anime `json:"node"`
		} `json:"data"`
	}
	json.Unmarshal(body, &res)

	for _, el := range res.Data {
		*animes = append(*animes, el.Anime)
	}

	return nil
}
