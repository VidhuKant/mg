/* mg - MyAnimeList to Go API wrapper
 * Copyright (C) 2023  Vidhu Kant Sharma <vidhukant@vidhukant.com>

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package mg

import "errors"

var (
	ErrLimitOutOfRange        = errors.New("mg: invalid limit (out of range)")
	ErrInvalidField           = errors.New("mg: invalid field passed")
	ErrInvalidRankingType     = errors.New("mg: invalid ranking type")
	ErrInvalidSeason          = errors.New("mg: invalid season")
	ErrInvalidSort            = errors.New("mg: invalid sort")
	ErrInvalidStatus          = errors.New("mg: invalid status")
	ErrInvalidScore           = errors.New("mg: invalid score")
	ErrInvalidPriority        = errors.New("mg: invalid priority")
	ErrInvalidRewatchValue    = errors.New("mg: invalid rewatch value")
	ErrInvalidRereadValue     = errors.New("mg: invalid reread value")
	ErrEmptySearchString      = errors.New("mg: invalid search string (empty string)")
	ErrInvalidYear            = errors.New("mg: invalid year (not integer)")
	ErrUnknownUpdateParam     = errors.New("mg: unknown update param")
	ErrAnimeNotInList         = errors.New("mg: anime is already not in list")
	ErrClientAuthNotSupported = errors.New("mg: function does not support client auth")
)
