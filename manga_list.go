/* mg - MyAnimeList to Go API wrapper
 * Copyright (C) 2023  Vidhu Kant Sharma <vidhukant@vidhukant.com>

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package mg

import (
	"encoding/json"
	"net/url"
	"fmt"
)

type MangaUpdateResponse struct {
	Status       string `json:"status"`
	Score        int    `json:"score"`
	VolumesRead  int    `json:"num_volumes_read"`
	ChaptersRead int    `json:"num_chapters_read"`
	IsRereading  bool   `json:"is_rereading"`
	StartDate    string `json:"start_date"`
	FinishDate   string `json:"finish_date"`
	Priority     string `json:"priority"`
	TimesReread  int    `json:"num_times_reread"`
	RereadValue  int    `json:"reread_value"`
	Tags         string `json:"tags"`
	Comments     string `json:"comments"`
	UpdatedAt    string `json:"updated_at"`
}

func (c Client) DeleteManga(id int) error {
	return c.delete(MANGA_BASE_URL + getDeleteQuery(id))
}

func (c Client) GetMangaList(mangas *[]Manga, params *ListParams) (bool, error) {
	err := validateMangaListParams(params)
	if err != nil {
		return false, err
	}

	params.Fields = append(params.Fields, "list_status")

	var res struct {
		Data []struct {
			Manga      Manga           `json:"node"`
			ListStatus MangaListStatus `json:"list_status"`
		} `json:"data"`
		Paging struct {
			NextPage string `json:"next"`
			// PrevPage string `json:"previous"`
		} `json:"paging"`
	}

	body, err := c.get(BASE_URL + getListQuery(params, "manga"))
	if err != nil {
		return false, err
	}
	json.Unmarshal(body, &res)

	for _, el := range res.Data {
		el.Manga.ListStatus = el.ListStatus
		*mangas = append(*mangas, el.Manga)
	}

	// returns true if there is a next page
	return res.Paging.NextPage != "", nil
}

func (c Client) UpdateManga(res *MangaUpdateResponse, id int, params map[string]interface{}) error {
	p := url.Values{}
	for k, v := range params {
		vString := fmt.Sprint(v)
		var err error

		// validate params
		switch(k) {
		case Status:
			err = validateAnimeListStatus(vString)
		case Score:
			err = validateScore(v.(int))
		case Priority:
			err = validatePriority(v.(int))
		case RereadValue:
			err = validateRereadValue(v.(int))
		case Tags, Comments, IsRereading, ChaptersRead, VolumesRead, TimesReread:
			// these ones don't need to be validated
			err = nil
		default:
			err = ErrUnknownUpdateParam
		}

		if err != nil {
			return err
		}

		p.Set(k, vString)
	}

	body, err := c.put(getUpdateQuery(id, "manga"), p)
	if err != nil {
		return err
	}
	json.Unmarshal(body, &res)

	return nil
}
