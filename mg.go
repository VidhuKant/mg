/* mg - MyAnimeList to Go API wrapper
 * Copyright (C) 2023  Vidhu Kant Sharma <vidhukant@vidhukant.com>

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package mg

const BASE_URL string = "https://api.myanimelist.net/v2"

// for UpdateAnime/UpdateManga
const (
	// used by UpdateAnime
	IsRewatching    = "is_rewatching"
	EpisodesWatched = "num_watched_episodes"
	TimesRewatched  = "num_times_rewatched"
	RewatchValue    = "rewatch_value"

	// used by UpdateManga
	IsRereading  = "is_rereading"
	VolumesRead  = "num_volumes_read"
	ChaptersRead = "num_chapters_read"
	TimesReread  = "num_times_reread"
	RereadValue  = "reread_value"

	// used by both
	Status   = "status"
	Score    = "score"
	Priority = "priority"
	Tags     = "tags"
	Comments = "comments"
)

// for getting ranking list
const (
	// for anime only
	RankingTypeAiring   = "airing"
	RankingTypeUpcoming = "upcoming"
	RankingTypeTV       = "tv"
	RankingTypeOVA      = "ova"
	RankingTypeMovie    = "movie"
	RankingTypeSpecial  = "special"

	// for manga only
	RankingTypeManga   = "manga"
	RankingTypeNovel   = "novels"
	RankingTypeOneShot = "oneshots"
	RankingTypeDoujin  = "doujin"
	RankingTypeManhwa  = "manhwa"
	RankingTypeManhua  = "manhua"

	// for both
	RankingTypeAll          = "all"
	RankingTypeByPopularity = "bypopularity"
	RankingTypeFavorite     = "favorite"
)

// for anime/manga list sort
const (
	// for anime only
	SortByAnimeTitle     = "anime_title"
	SortByAnimeStartDate = "anime_start_date"
	SortByAnimeId        = "anime_id"

	// for manga only
	SortByMangaTitle     = "manga_title"
	SortByMangaStartDate = "manga_start_date"
	SortByMangaId        = "manga_id"

	// for both
	SortByListScore     = "list_score"
	SortByListUpdatedAt = "list_updated_at"
)

// for anime/manga list status
const (
	// for anime only
	ListStatusWatching = "watching"
	ListStatusPTW      = "plan_to_watch"

	// for manga only
	ListStatusReading = "reading"
	ListStatusPTR     = "plan_to_read"

	// for both
	ListStatusCompleted = "completed"
	ListStatusOnHold    = "on_hold"
	ListStatusDropped   = "dropped"
	ListStatusAll       = "" // for getting user lists only
)

// for anime seasons
const (
	// season names
	SeasonWinter = "winter"
	SeasonSpring = "spring"
	SeasonSummer = "summer"
	SeasonFall   = "fall"

	// sorting
	SeasonSortByAnimeScore   = "anime_score"
	SeasonSortByNumListUsers = "num_list_users"
)
