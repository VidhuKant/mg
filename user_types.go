/* mg - MyAnimeList to Go API wrapper
 * Copyright (C) 2023  Vidhu Kant Sharma <vidhukant@vidhukant.com>

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package mg

type User struct {
	Id              int                 `json:"id"`
	Name            string              `json:"name"`
	Picture         string              `json:"picture"`
	Gender          string              `json:"gender"`
	Birthday        string              `json:"birthday"`
	Location        string              `json:"location"`
	JoinedAt        string              `json:"joined_at"`
	TimeZone        string              `json:"time_zone"`
	IsSupporter     bool                `json:"is_supporter"`
	AnimeStatistics UserAnimeStatistics `json:"anime_statistics"`
}

type UserAnimeStatistics struct {
	NumWatching    int     `json:"num_items_watching"`
	NumCompleted   int     `json:"num_items_completed"`
	NumOnHold      int     `json:"num_items_on_hold"`
	NumDropped     int     `json:"num_items_dropped"`
	NumPlanned     int     `json:"num_items_plan_to_watch"`
	TotalAnimes    int     `json:"num_items"`
	DaysWatched    float32 `json:"num_days_watched"`
	DaysWatching   float32 `json:"num_days_watching"`
	DaysCompleted  float32 `json:"num_days_completed"`
	DaysOnHold     float32 `json:"num_days_on_hold"`
	DaysDropped    float32 `json:"num_days_dropped"`
	TotalDays      float32 `json:"num_days"`
	TotalEpisodes  int     `json:"num_episodes"`
	TimesRewatched int     `json:"num_times_rewatched"`
	MeanScore      float32 `json:"mean_score"`
}
