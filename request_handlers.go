/* mg - MyAnimeList to Go API wrapper
 * Copyright (C) 2023  Vidhu Kant Sharma <vidhukant@vidhukant.com>

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package mg

import (
	"encoding/json"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"errors"
	"io"
)

func (c Client) get(endpoint string) ([]byte, error) {
	// can safely ignore error with http.NewRequest
	req, _ := http.NewRequest(http.MethodGet, endpoint, nil)

	// add authorization headers
	if c.ClientAuthOnly {
		req.Header.Add("X-MAL-CLIENT-ID", c.ClientAuth)
	} else {
		req.Header.Add("Authorization", c.MainAuth)
	}

	// send request
	res, err := c.httpClient.Do(req)
	if err != nil {
		return []byte{}, err
	}
	defer res.Body.Close()

	// read response body
	body, err := io.ReadAll(res.Body)
	if err != nil {
		return body, err
	}

	// in case MAL returns error
	var e MALError
	json.Unmarshal(body, &e)
	if e.Err != "" {
		return body, errors.New("MyAnimeList Error: " + e.Err + " " + e.Msg)
	}

	return body, nil
}

func (c Client) delete(endpoint string) error {
	if c.ClientAuthOnly {
		return ErrClientAuthNotSupported
	}

	// can safely ignore error with http.NewRequest
	req, _ := http.NewRequest(http.MethodDelete, endpoint, nil)

	// add authorization headers
	req.Header.Add("Authorization", c.MainAuth)

	// send request
	res, err := c.httpClient.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	// read response body
	body, err := io.ReadAll(res.Body)
	if err != nil {
		return err
	}

	// in case MAL returns error
	var e MALError
	json.Unmarshal(body, &e)
	if e.Err != "" {
		return errors.New("MyAnimeList Error: " + e.Err + " " + e.Msg)
	}

	// if anime wasn't in the list from the start
	if res.StatusCode == 404 {
		return ErrAnimeNotInList
	}

	return nil
}

func (c Client) put(endpoint string, params url.Values) ([]byte, error) {
	if c.ClientAuthOnly {
		return []byte{}, ErrClientAuthNotSupported
	}

	encodedParams := params.Encode()

	// can safely ignore error with http.NewRequest
	req, _ := http.NewRequest(http.MethodPatch, endpoint, strings.NewReader(encodedParams))

	// add authorization headers
	req.Header.Add("Authorization", c.MainAuth)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Content-Length", strconv.Itoa(len(encodedParams)))

	// send request
	res, err := c.httpClient.Do(req)
	if err != nil {
		return []byte{}, err
	}
	defer res.Body.Close()

	// read response body
	body, err := io.ReadAll(res.Body)
	if err != nil {
		return body, err
	}

	// in case MAL returns error
	var e MALError
	json.Unmarshal(body, &e)
	if e.Err != "" {
		return body, errors.New("MyAnimeList Error: " + e.Err + " " + e.Msg)
	}

	return body, nil
}
