/* mg - MyAnimeList to Go API wrapper
 * Copyright (C) 2023  Vidhu Kant Sharma <vidhukant@vidhukant.com>

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package mg

import "encoding/json"

const MANGA_BASE_URL string = BASE_URL + "/manga"

func (c Client) SearchManga(mangas *[]Manga, params *SearchParams) error {
	err := validateMangaSearchParams(params)
	if err != nil {
		return err
	}

	var res struct {
	  Data []struct {
	  	Manga Manga `json:"node"`
	  } `json:"data"`
	}

	body, err := c.get(MANGA_BASE_URL + getSearchQuery(params))
	if err != nil {
		return err
	}
	json.Unmarshal(body, &res)

	for _, el := range res.Data {
		*mangas = append(*mangas, el.Manga)
	}

	return nil
}

func (c Client) GetMangaById(manga *Manga, id int, fields []string) error {
	err := validateMangaFields(&fields)
	if err != nil {
		return err
	}

	body, err := c.get(ANIME_BASE_URL + getIdQuery(id, fields))
	if err != nil {
		return err
	}
	json.Unmarshal(body, manga)

	return nil
}

func (c Client) GetMangaRanking(mangas *[]RankedManga, params *RankingParams) error {
	err := validateMangaRankingParams(params)
	if err != nil {
		return err
	}

	body, err := c.get(MANGA_BASE_URL + getRankingQuery(params))
	if err != nil {
		return err
	}

	var res struct {
	  Data []struct {
	  	Manga RankedManga `json:"node"`
	  	Ranking struct {
	  		Rank int `json:"rank"`
	  	} `json:"ranking"`
	  } `json:"data"`
  }
	json.Unmarshal(body, &res)

	for _, el := range res.Data {
		el.Manga.RankNum = el.Ranking.Rank
		*mangas = append(*mangas, el.Manga)
	}

	return nil
}
