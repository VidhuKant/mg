/* mg - MyAnimeList to Go API wrapper
 * Copyright (C) 2023  Vidhu Kant Sharma <vidhukant@vidhukant.com>

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package mg

var DefaultAnimeFields []string = []string {
	"id",
	"title",
	"main_picture",
	"alternative_titles",
	"start_date",
	"end_date",
	"synopsis",
	"mean",
	"rank",
	"popularity",
	"num_list_users",
	"num_scoring_users",
	"nsfw",
	"created_at",
	"updated_at",
	"media_type",
	"status",
	"genres",
	"my_list_status",
	"list_status",
	"num_episodes",
	"start_season",
	"broadcast",
	"source",
	"average_episode_duration",
	"rating",
	"pictures",
	"background",
	"related_anime",
	"related_manga",
	"recommendations",
	"studios",
	"statistics",
}

var DefaultMangaFields []string = []string {
	"id",
	"title",
	"main_picture",
	"alternative_titles",
	"start_date",
	"end_date",
	"synopsis",
	"mean",
	"rank",
	"popularity",
	"num_list_users",
	"num_scoring_users",
	"nsfw",
	"created_at",
	"updated_at",
	"media_type",
	"status",
	"genres",
	"my_list_status",
	"num_volumes",
	"num_chapters",
	"authors{first_name,last_name}",
	"pictures",
	"background",
	"related_anime",
	"related_manga",
	"recommendations",
	"serialization",
}
