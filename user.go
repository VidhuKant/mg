/* mg - MyAnimeList to Go API wrapper
 * Copyright (C) 2023  Vidhu Kant Sharma <vidhukant@vidhukant.com>

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package mg

import "encoding/json"

func (c Client) GetSelfInfo(user *User, getStatistics bool) error {
	if c.ClientAuthOnly {
		// this endpoint requires main auth
		return ErrClientAuthNotSupported
	}

	endpoint := BASE_URL + "/users/@me"
	if getStatistics {
		endpoint += "?fields=anime_statistics"
	}

	body, err := c.get(endpoint)
	if err != nil {
		return err
	}
	json.Unmarshal(body, &user)

	return nil
}
