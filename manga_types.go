/* mg - MyAnimeList to Go API wrapper
 * Copyright (C) 2023  Vidhu Kant Sharma <vidhukant@vidhukant.com>

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package mg

type Manga struct {
	Id              int                   `json:"id"`
	Title           string                `json:"title"`
	MainPicture     Picture               `json:"main_picture"`
	AltTitles       AltTitles             `json:"alternative_titles"`
	StartDate       string                `json:"start_date"`
	EndDate         string                `json:"end_date"`
	Synopsis        string                `json:"synopsis"`
	MeanScore       float32               `json:"mean"`
	Rank            int                   `json:"rank"`
	Popularity      int                   `json:"popularity"`
	NumListUsers    int                   `json:"num_list_users"`
	NSFWStatus      string                `json:"nsfw"`
	Genres          []Genre               `json:"genres"`
	CreatedAt       string                `json:"created_at"`
	UpdatedAt       string                `json:"updated_at"`
	MediaType       string                `json:"media_type"`
	Status          string                `json:"status"`
	MyListStatus    MangaListStatus       `json:"my_list_status"`
	ListStatus      MangaListStatus       `json:"list_status"`
	NumVolumes      int                   `json:"num_volumes"`
	NumChapters     int                   `json:"num_chapters"`
	Authors         []Author              `json:"authors"`
	Pictures        []Picture             `json:"pictures"`
	Background      string                `json:"background"`
	RelatedAnime    []RelatedAnime        `json:"related_anime"`
	RelatedManga    []RelatedManga        `json:"related_manga"`
	Recommendations []MangaRecommendation `json:"recommendations"`
	Serialization   []Serialization       `json:"serialization"`
}

type RankedManga struct {
	Manga
	RankNum int
}

type Magazine struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

type Serialization struct {
	Magazine Magazine `json:"node"`
	Name     string   `json:"name"`
}

type MangaRecommendation struct {
	Manga            Anime `json:"node"`
	TimesRecommended int   `json:"num_recommendations"`
}

type Author struct {
	Details struct {
		Id        int    `json:"id"`
		FirstName string `json:"first_name"`
		LastName  string `json:"last_name"`
	} `json:"node"`
	Role      string `json:"role"`
}

type MangaListStatus struct {
	listStatus
	VolumesRead  int  `json:"num_volumes_read"`
	ChaptersRead int  `json:"num_chapters_read"`
	IsRereading  bool `json:"is_rereading"`
	TimesReread  int  `json:"num_times_reread"`
	RereadValue  int  `json:"reread_value"`
}

type RelatedManga struct {
	Manga                 Manga  `json:"node"`
	RelationType          string `json:"relation_type"`
	RelationTypeFormatted string `json:"relation_type_formatted"`
}
