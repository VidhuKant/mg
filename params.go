/* mg - MyAnimeList to Go API wrapper
 * Copyright (C) 2023  Vidhu Kant Sharma <vidhukant@vidhukant.com>

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package mg

type SearchParams struct {
	Limit  int
	Offset int
	NSFW   bool
	Fields []string

	SearchString string
}

type RankingParams struct {
	Limit  int
	Offset int
	NSFW   bool
	Fields []string

	RankingType string
}

type SeasonalParams struct {
	Limit  int
	Offset int
	NSFW   bool
	Fields []string

	Year   string
	Season string
	Sort   string
}

type SuggestedParams struct {
	Limit  int
	Offset int
	NSFW   bool
	Fields []string
}

type ListParams struct {
	Username string
	Status   string
	Sort     string
	Limit    int
	Offset   int
	NSFW     bool
	Fields   []string
}

func SearchParamsNew() *SearchParams {
	return &SearchParams {
	  SearchString: "",
	  Limit: 100,
	  Offset: 0,
	  NSFW: false,
		Fields: []string{},
	}
}

func RankingParamsNew() *RankingParams {
	return &RankingParams {
	  RankingType: RankingTypeAll,
	  Limit: 100,
	  Offset: 0,
	  NSFW: false,
		Fields: []string{},
	}
}

func SeasonalParamsNew() *SeasonalParams {
	return &SeasonalParams {
	  Year: "",
		Season: "",
		Sort: SeasonSortByAnimeScore,
	  Limit: 100,
	  Offset: 0,
	  NSFW: false,
		Fields: []string{},
	}
}

func SuggestedParamsNew() *SuggestedParams {
	return &SuggestedParams {
	  Limit: 100,
	  Offset: 0,
	  NSFW: false,
		Fields: []string{},
	}
}

func ListParamsNew() *ListParams {
	return &ListParams {
	  Username: "@me",
		Status: "",
		Sort: "list_score",
	  Limit: 100,
	  Offset: 0,
	  NSFW: false,
		Fields: []string{},
	}
}
