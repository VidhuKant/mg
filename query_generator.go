/* mg - MyAnimeList to Go API wrapper
 * Copyright (C) 2023  Vidhu Kant Sharma <vidhukant@vidhukant.com>

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package mg

import (
	"net/url"
	"reflect"
	"strconv"
	"fmt"
)

type query struct {
	key string
	value interface{}
}

func queryString(queries []query) string {
	var queryString string

	for i, j := range queries {
		var q string
		/* if the query is the first field in URL,
		 * it goes like ?key=value
		 * else it is &next_key=value
		 */
		if i == 0 {
			q += "?" + j.key + "="
		} else {
			q += "&" + j.key + "="
		}

		// if value is a slice, append each element with a comma
		if reflect.TypeOf(j.value).Kind() == reflect.Slice {
		  for x, y := range j.value.([]string) {
		  	if x > 0 {
		  		q += "," + url.QueryEscape(y)
		  	} else {
		  		q += url.QueryEscape(y)
		  	}
		  }
		} else {
			q += url.QueryEscape(fmt.Sprint(j.value))
		}

		queryString += q
	}

	return queryString
}

func getSearchQuery(params *SearchParams) string {
	return queryString([]query {
		{"q",      params.SearchString},
		{"limit",  params.Limit},
		{"offset", params.Offset},
		{"nsfw",   params.NSFW},
		{"fields", params.Fields},
	})
}

func getIdQuery(id int, fields []string) string {
	return "/" + strconv.Itoa(id) + queryString([]query {
		{"fields", fields},
	})
}

func getRankingQuery(params *RankingParams) string {
	return "/ranking" + queryString([]query {
		{"ranking_type", params.RankingType},
		{"limit",        params.Limit},
		{"offset",       params.Offset},
		{"nsfw",         params.NSFW},
		{"fields",       params.Fields},
	})
}

func getSeasonalQuery(params *SeasonalParams) string {
	return "/season/" + params.Year + "/" + params.Season + queryString([]query {
		{"sort",   params.Sort},
		{"limit",  params.Limit},
		{"offset", params.Offset},
		{"nsfw",   params.NSFW},
		{"fields", params.Fields},
	})
}

func getSuggestedQuery(params *SuggestedParams) string {
	return "/suggestions" + queryString([]query {
		{"limit",  params.Limit},
		{"offset", params.Offset},
		{"nsfw",   params.NSFW},
		{"fields", params.Fields},
	})
}

func getDeleteQuery(id int) string {
	return "/" + strconv.Itoa(id) + "/my_list_status"
}

func getListQuery(params *ListParams, t string) string {
	q := []query {
		{"sort",   params.Sort},
		{"limit",  params.Limit},
		{"offset", params.Offset},
		{"nsfw",   params.NSFW},
		{"fields", params.Fields},
	}

	// only add status query if given
	if params.Status != "" {
		q = append(q, query{"status", params.Status})
	}

	return "/users/" + params.Username + "/" + t + "list" + queryString(q)
}

func getUpdateQuery(id int, t string) string {
	return BASE_URL + "/" + t + "/" + strconv.Itoa(id) + "/my_list_status"
}
