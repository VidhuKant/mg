# mg

mg is a free and open source library for accessing the official MyAnimeList V2 API using Go. All endpoints are supported except forums as of now.

Official website & docs: <https://mg.vidhukant.com>

If you find any bugs, please write to me at <vidhukant@vidhukant.com>

# License

Licenced under GNU General Public Licence V3

GNU GPL License: [LICENSE](https://mikunonaka.net/mg/tree/LICENSE)

Copyright (c) 2022-2023 Vidhu Kant Sharma
