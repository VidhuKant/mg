/* mg - MyAnimeList to Go API wrapper
 * Copyright (C) 2023  Vidhu Kant Sharma <vidhukant@vidhukant.com>

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package mg

type Anime struct {
	Id              int                   `json:"id"`
	Title           string                `json:"title"`
	MainPicture     Picture               `json:"main_picture"`
	AltTitles       AltTitles             `json:"alternative_titles"`
	StartDate       string                `json:"start_date"`
	EndDate         string                `json:"end_date"`
	Synopsis        string                `json:"synopsis"`
	MeanScore       float32               `json:"mean"`
	Rank            int                   `json:"rank"`
	Popularity      int                   `json:"popularity"`
	NumListUsers    int                   `json:"num_list_users"`
	NumScoringUsers int                   `json:"num_scoring_users"`
	NSFWStatus      string                `json:"nsfw"`
	CreatedAt       string                `json:"created_at"`
	UpdatedAt       string                `json:"updated_at"`
	MediaType       string                `json:"media_type"`
	Status          string                `json:"status"`
	Genres          []Genre               `json:"genres"`
	MyListStatus    AnimeListStatus       `json:"my_list_status"`
	ListStatus      AnimeListStatus       `json:"list_status"`
	NumEpisodes     int                   `json:"num_episodes"`
	Season          Season                `json:"start_season"`
	Broadcast       Broadcast             `json:"broadcast"`
	Source          string                `json:"source"`
	DurationSeconds int                   `json:"average_episode_duration"`
	ParentalRating  string                `json:"rating"`
	Pictures        []Picture             `json:"pictures"`
	Background      string                `json:"background"`
	RelatedAnime    []RelatedAnime        `json:"related_anime"`
	Recommendations []AnimeRecommendation `json:"recommendations"`
	Studios         []Studio              `json:"studios"`
	Statistics      AnimeStatistics       `json:"statistics"`
}

type RankedAnime struct {
	Anime
	RankNum int
}

type Season struct {
	Year int    `json:"year"`
	Name string `json:"season"`
}

type Studio struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

type AnimeStatistics struct {
	NumListUsers int              `json:"num_list_users"`
	Status       StatusStatistics `json:"status"`
}

type Broadcast struct {
	Day  string `json:"day_of_the_week"`
	Time string `json:"start_time"`
}

type AnimeRecommendation struct {
	Anime            Anime `json:"node"`
	TimesRecommended int   `json:"num_recommendations"`
}

type AnimeListStatus struct {
	listStatus
	EpWatched      int  `json:"num_episodes_watched"`
	IsRewatching   bool `json:"is_rewatching"`
	TimesRewatched int  `json:"num_times_rewatched"`
	RewatchValue   int  `json:"rewatch_value"`
}

type RelatedAnime struct {
	Anime                 Anime  `json:"node"`
	RelationType          string `json:"relation_type"`
	RelationTypeFormatted string `json:"relation_type_formatted"`
}
